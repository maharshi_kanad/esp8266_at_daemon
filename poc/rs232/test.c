/**************************************************

file: main.c
purpose: simple demo that receives characters from
the serial port and print them on the screen,
exit the program by pressing Ctrl-C

**************************************************/

#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include "rs232.h"


int main(int argc, char *argv[])
{
  int i, n,
      cport_nr=0,        /* /dev/ttyS0 (COM1 on windows) */
      bdrate=115200;       /* 9600 baud */

  unsigned char buf[4096];
  
    char mode[]={'8','N','1',0};
  
  //char tx_bu[] = {'A', 'T', '+', 'G', 'M', 'R', '\r', 0x0a}; SDK version
    //AT+CWMODE=3
  //char tx_bu[] = {'A', 'T', '+', 'G', 'W', 'M', 'O', 'D', 'E', '=', '3', '\r', 0x0a};
    
    // AT+CWJAP?
    //char tx_bu[] = {'A', 'T', '+', 'C', 'W', 'J', 'A', 'P', '?', '\r', 0x0a};
    
    //AT+CWLAP
  //char tx_bu[] = {'A', 'T', '+', 'C', 'W', 'L', 'A', 'P', '\r', 0x0a};
    
    char tx_bu[] = {'H', 'I', '\r', 0x0a};
  
  if(argc >= 2)
    cport_nr = atoi(argv[1]);

  printf("openig com port %d", cport_nr);
  if(RS232_OpenComport(cport_nr, bdrate, mode))
  {
    printf("Can not open comport\n");

    return(0);
  }

  while(1)
  {
    n = RS232_PollComport(cport_nr, buf, 4095);

	
    if(n > 0)
    {
        printf("received %i \n", n);
        
        sleep(1);
        i= 0 ;
        while(n)
        {
        printf("0x%2X %c\n",(int) buf[i], buf[i]);
        n--;
        i++;
        }
        break;
    }
    

	RS232_SendBuf(cport_nr, tx_bu, sizeof(tx_bu));
#ifdef _WIN32
    Sleep(100);
#else
    sleep(10);
    usleep(100000);  /* sleep for 100 milliSeconds */
#endif
  }

  return(0);
}

